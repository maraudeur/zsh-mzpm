#!/usr/bin/env zsh

MZPM_HOME="${0:A:h}"
MZPM_STATUS_FILE="${MZPM_HOME}/status.local"
MZPM_PLUGINS_DIR="${HOME}/.zsh/plugins"

[[ ! -d "${MZPM_PLUGINS_DIR}" ]] && command mkdir -p "${MZPM_PLUGINS_DIR}"

mzpm::error() {
  printf "%s\n" "$1" >&2
}

mzpm::get_plugins() {
  unset MZPM_PLUGINS
  typeset -gA MZPM_PLUGINS

  for dir in "${MZPM_PLUGINS_DIR}"/*; do
    [[ -d "${dir}" ]] && MZPM_PLUGINS[${dir##*/}]=0
  done
  [[ -f "$MZPM_STATUS_FILE" ]] && {
    for plugin in $(cat "$MZPM_STATUS_FILE"); do
      MZPM_PLUGINS[$plugin]=1
    done
  }
}

mzpm::load_plugin() {
  local plugin="$1"
  local plugin_path="${MZPM_PLUGINS_DIR}/${plugin}"
  if [[ -f "${plugin_path}/zsh-${plugin}.plugin.zsh" ]]; then
    source "${plugin_path}/zsh-${plugin}.plugin.zsh"
  elif [[ -f "${plugin_path}/${plugin}.plugin.zsh" ]]; then
    source "${plugin_path}/${plugin}.plugin.zsh"
  elif [[ -f "${plugin_path}/${plugin}.zsh" ]]; then
    source "${plugin_path}/${plugin}.zsh"
  fi
}

mzpm::load() {
  for plugin in "${(@k)MZPM_PLUGINS}"; do
    (( "${MZPM_PLUGINS[$plugin]:-0}" == 1 )) && mzpm::load_plugin ${plugin}
  done
}

mzpm::toggle() {
  local plugin="$1"
  [[ -z "$plugin" ]] && {
    mzpm::error "Parameter missing!"
    return 1
  }
  [[ -d "${MZPM_PLUGINS_DIR}/${plugin}" ]] && {
    local -i new_status=$2
    (( new_status != "${MZPM_PLUGINS[$plugin]:-0}" )) && {
      if (( new_status == 1 )); then
        echo "$plugin" >> "$MZPM_STATUS_FILE"
        printf "%s %s\n" "${plugin}" "enabled"
        MZPM_PLUGINS[$plugin]=1
        mzpm::load_plugin "$plugin"
      else
        command sed -i "/${plugin}/d" "${MZPM_STATUS_FILE}"
        printf "%s %s\n" "${plugin}" "disabled"
      fi
      return 0
    }
    return 1
  }
  mzpm::error "${plugin} not installed!"
  return 1
}

mzpm::parse_url() {
  local url="$1"
  if [[ "$url" =~ ^(https://|git@)git(hub|lab)\.com/[-\.a-zA-Z0-9]+/[-\.a-zA-Z0-9]+(|\.git)$ ]]; then
    url="$url"
  elif [[ "$url" =~ ^git(hub|lab)\.com/[-\.a-zA-Z0-9]+/[-\.a-zA-Z0-9]+(|\.git)$ ]]; then
    url="https://${url}"
  elif [[ "$url" =~ ^git(hub|lab)/[-\.a-zA-Z0-9]+/[-\.a-zA-Z0-9]+$ ]]; then
    url="https://${url%%/*}.com/${url#*/}.git"
  elif [[ "$url" =~ ^[-\.a-zA-Z0-9]+/[-\.a-zA-Z0-9]+$ ]]; then
    url="https://github.com/${url}.git"
  else
    mzpm::error "Invalid url: ${url}."
    return 1
  fi

  case "$url" in
    *".git") ;;
    *) result="${url}.git" ;;
  esac
  echo "$url"
}

mzpm::install() {
  local url="$1"
  [[ -z "$url" ]] && {
    mzpm::error "URL is missing."
    return 1
  }

  local url=$(mzpm::parse_url "$url")
  local plugin="${${url##*/}%.git}"
  local plugin_dir="${MZPM_PLUGINS_DIR}/${plugin}"
  [[ -d "${plugin_dir}" ]] && {
    mzpm::error "${plugin} (${plugin_dir}) already exists."
    return 1
  }
  printf "%s: Downloading...\r" "${plugin}"
  command git clone "${url}" "${plugin_dir}" >/dev/null 2>&1 && {
    [[ -d "${MZPM_PLUGINS_DIR}/${plugin}" ]] && {
      printf "%s: Done!%-15s\n" "${plugin}"
      mzpm::get_plugins
      return
    }
  }
  mzpm::error "${plugin}: something went wrong.%-15"
  return 1
}

mzpm::update() {
  local plugin="$1"
  [[ -z "$plugin" ]] && {
    mzpm::error "Plugin name is missing."
    return 1
  }

  pushd -q "${MZPM_PLUGINS_DIR}/${plugin}" >/dev/null 2>&1 && {
    command git rev-parse --git-dir >/dev/null 2>&1 && {
      (( $(command git remote | wc -l) != 0 )) && {
        local local_commit=$(command git log --pretty=format:%h -1)
        command git pull --rebase --quiet >/dev/null 2>&1 && {
          local new_commit=$(command git log --pretty=format:%h -1)
          [[ $local_commit != $new_commit ]] && {
            print "$plugin"
            local since=$(git log "$local_commit" -1 --pretty=format:%cI)
            command git log --oneline --pretty=format:"%h %s" --since="$since"
          }
        }
      }
    }
    pushd -q -
  }
}

mzpm::remove() {
  local plugin="$1"
  [[ -z "$plugin" ]] && {
    mzpm::error "Plugin name is missing."
    return 1
  }
  local plugin_dir="${MZPM_PLUGINS_DIR}/${plugin}"
  [[ ! -d "${plugin_dir}" ]] && {
    mzpm::error "${plugin} not installed."
    return 1
  }
  local reply
  print "${plugin} (${plugin_dir}) will be removed."
  read -q "?Are you sure? [y/n] " reply
  printf "\n"
  [[ "${(L)reply}" == "y" ]] && {
    command rm -rf "${plugin_dir}" && {
      command sed -i "/${plugin}/d" "${MZPM_STATUS_FILE}"
      print "${plugin} removed."
      mzpm::get_plugins
      return 0
    }
  }
  return 1
}

mzpm::list() {
  for plugin in "${(@k)MZPM_PLUGINS}"; do
    printf "%-30s" "${plugin}"
    if (( "${MZPM_PLUGINS[$plugin]:-0}" == 1 )); then
      print "\e[32menabled\e[m"
    else
      print "\e[31mdisabled\e[m"
    fi
  done
}

mzpm() {
  (( $# == 0 )) && return 1

  local cmd="$1"; shift
  local -a commands=(
    "list"
    "install"
    "remove"
    "enable"
    "disable"
    "update"
  )
  [[ "$cmd" == "list" ]] && {
    mzpm::list
    return
  }

  local valid=false
  for c in "${commands[@]}"; do
    [[ "$c" == "$cmd" ]] && {
      valid=true
      break
    }
  done
  unset c

  [[ $valid == false ]] && {
    mzpm::error "unknown command: ${cmd}"
    return 1
  }

  local func="${cmd/*able/toggle}"
  local arg="${${${cmd/enable/1}/disable/0}#${cmd}}"
  for plugin in "${@:-}"; do
    mzpm::${func} "$plugin" "$arg"
  done
}

() {
  mzpm::get_plugins
  mzpm::load
}
