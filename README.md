## zsh-mzpm

Yet another plugin manager for ZSH. Why? ¯\\_(ツ)_/¯

## Installation and Usage
### Install with `git`
```shell
$ git clone https://codeberg.org/maraudeur/zsh-mzpm.git ~/.zsh/zsh-mzpm
$ echo "source ~/.zsh/zsh-mzpm/zsh-mzpm.plugin.zsh" >> ~/.zshrc
```

### Usage
```shell
$ mzpm COMMAND [plugin]
```
Install plugin, e.g. [zsh-autopair](https://github.com/hlissner/zsh-autopair)
```shell
$ mzpm install github.com/hlissner/zsh-autopair
```
or just
```shell
$ mzpm install hlissner/zsh-autopair
```
then enable it.
```shell
$ mzpm enable zsh-autopair
```
#### Available commands
* list
* enable
* disable
* install
* remove
* update

## License
[MIT](./LICENSE)
